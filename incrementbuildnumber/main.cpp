#include <iostream>
#include <fstream>

int main(int argc, const char * argv[])
{
    if (argc < 2)
    {
        std::cout << "You must enter filename!!!" << std::endl;
        
        return 0;
    }
    
    long long id;
    {
        std::ifstream in( argv[1] );
        in >> id;
    }
    id++;
    {
        std::ofstream out( argv[1] );
        out << id;
    }
    
    return 0;
}
